<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class MovimentacaoController extends CI_Controller {

        function __construct()
        {
            parent::__construct();
            
            date_default_timezone_set('America/Sao_Paulo');
            $this->load->model("Movimentacao", "movimentacao", true);

            if (usuario_logado() === FALSE)
            {
                redirect(base_url('usuarios/login'));
            }
        }
      
        public function formCadastroMovimentacao(){
           $this->load->view('movimentacao/cadastrar_movimentacao');
        }

        public function inserirMovimentacao(){
            $this->form_validation->set_rules('descricao', 'Descrição', 'required');
            $this->form_validation->set_rules('tipo', 'Tipo', 'required');
            $this->form_validation->set_rules('data', 'Data', 'required');
            $this->form_validation->set_rules('valor', 'Valor', 'required|numeric');
            
            if($this->form_validation->run() == false){
                $this->load->view('movimentacao/cadastrar_movimentacao');
            }else{
                $config['upload_path']          = './uploads/comprovantes/';
                $config['allowed_types']        = 'gif|jpg|png|pdf';

                $this->upload->initialize($config);
                
                $data = [
                    'descricao' => $this->input->post('descricao'),
                    'tipo' => $this->input->post('tipo'),
                    'data' => data_portugues_para_ingles($this->input->post('data')),
                    'valor' => $this->input->post('valor'),
                    'datahora_cadastro' => date('Y-m-d H:i:s'),
                    'id_usuario' => getDadosUsuarioLogado()['id']
                ];
                
                if (!$this->upload->do_upload('comprovante'))
                {
                    $errors = $this->upload->display_errors();
                    $this->session->set_flashdata('cadastro-movimentacao', $errors);
                    redirect(base_url('movimentacao/cadastrar'));
                }
                else
                {
                    $dados_upload = $this->upload->data();
                    $filename = $dados_upload['file_name'];
                    $data['arquivo_comprovante'] = $config['upload_path'] . $filename;
                }

                $movimentacao_id = $this->movimentacao->insert($data);
                if (!is_null($movimentacao_id))
                {
                    $this->session->set_flashdata('cadastro-movimentacao', "<p class='alert alert-success'>Movimentação cadastrada com sucesso.</p>");
                }
                else
                {
                    $this->session->set_flashdata('cadastro-movimentacao', "<p class='alert alert-danger'>Ocorreu erro ao salvar a movimentação.</p>");
                }
                redirect(base_url('movimentacao/cadastrar'));
            }
        }

        public function listarMovimentacoes(){
            $lista_movimentacoes = $this->movimentacao->getMovimentacoes(getDadosUsuarioLogado()['id']);

            $dados = array(
                "lista_movimentacoes" => $lista_movimentacoes
            );
            $this->load->view('movimentacao/listar_movimentacao', $dados);
        }

        public function excluirMovimentacao($movimentacao_id)
        {   
            $movimentacao = $this->movimentacao->buscarPorCodigo($movimentacao_id);
            if (!is_null($movimentacao))
            {
                if ($movimentacao->id_usuario !== getDadosUsuarioLogado()['id'])
                {
                    $this->session->set_flashdata('listar-movimentacao', "<p class='alert alert-danger'>Você não tem permissão para acessar este registro.</p>");
                    redirect(base_url('movimentacoes'));
                }
                $this->movimentacao->excluir($movimentacao_id);
                if (!empty($movimentacao->arquivo_comprovante) && !is_null($movimentacao->arquivo_comprovante)){
                    unlink($movimentacao->arquivo_comprovante);
                }
                $this->session->set_flashdata('lista-movimentacao', 'Movimentação excluída com sucesso.');
            }
            else
            {
                $this->session->set_flashdata('listar-movimentacao', 'Este registro não existe no banco de dados.');
            }

            redirect(base_url('movimentacoes'));
        }

        public function editarMovimentacao($movimentacao_id)
        {
            $movimentacao = $this->movimentacao->buscarPorCodigo($movimentacao_id);
            if ($movimentacao->id_usuario !== getDadosUsuarioLogado()['id'])
            {
                $this->session->set_flashdata('listar-movimentacao', "<p class='alert alert-danger'>Você não tem permissão para acessar este registro.</p>");
                redirect(base_url('movimentacoes'));
            }
            $dados = array(
                "movimentacao" => $movimentacao
            );
            $this->load->view('movimentacao/editar_movimentacao', $dados);
        }

        public function salvarMovimentacao($movimentacao_id)
        {
            $this->form_validation->set_rules('descricao', 'Descrição', 'required');
            $this->form_validation->set_rules('tipo', 'Tipo', 'required');
            $this->form_validation->set_rules('data', 'Data', 'required');
            $this->form_validation->set_rules('valor', 'Valor', 'required|numeric');

            if ($this->form_validation->run() === FALSE)
            {
                $this->editarMovimentacao($movimentacao_id);
            }
            else
            {
                $config['upload_path']          = './uploads/comprovantes/';
                $config['allowed_types']        = 'gif|jpg|png|pdf';

                $this->upload->initialize($config);

                $movimentacao = array(
                    "descricao" => $this->input->post("descricao"),
                    "tipo" => $this->input->post("tipo"),
                    'data' => data_portugues_para_ingles($this->input->post('data')),
                    "valor" => $this->input->post("valor"),
                    "datahora_ultimaalteracao" => date('Y-m-d H:i:s')
                );

                if (!empty($_FILES['comprovante']['name']))
                {
                    if (!$this->upload->do_upload('comprovante'))
                    {
                        $errors = $this->upload->display_errors();
                        $this->session->set_flashdata('edicao-movimentacao', $errors);
                        redirect(base_url("movimentacao/editar/{$movimentacao_id}"));
                    }
                    else
                    {
                        $dados_upload = $this->upload->data();
                        $filename = $dados_upload['file_name'];
                        $movimentacao['arquivo_comprovante'] = $config['upload_path'] . $filename;
                    }
                }

                $id = $this->movimentacao->atualizar($movimentacao_id, $movimentacao);
                if (is_null($id))
                {
                    $this->session->set_flashdata('edicao-movimentacao', 'Erro ao salvar as alterações da movimentação.');
                }
                else
                {
                    $this->session->set_flashdata('edicao-movimentacao', 'Alterações salvas com sucesso.');
                }

                redirect(base_url("movimentacao/editar/{$movimentacao_id}"));
            }
        }
    }

