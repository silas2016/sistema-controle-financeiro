<?php

function data_ingles_para_portugues($data_ingles)
{
    $array_data = explode('-', $data_ingles);
    return "{$array_data[2]}/{$array_data[1]}/{$array_data[0]}";
}

function data_portugues_para_ingles($data_portugues)
{
    $array_data = explode('/', $data_portugues);
    return "{$array_data[2]}-{$array_data[1]}-{$array_data[0]}";
}