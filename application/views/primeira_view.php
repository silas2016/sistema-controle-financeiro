<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-5 offset-3">
                <form action="">
                <div class="form-group">
                    <label for="">Descrição</label>
                    <input class="form-control" type="text" name="descricao" >
                </div>
                <div class="form-group">
                    <label for="">Valor</label>
                    <input class="form-control" type="text" name="valor" >
                </div>
                <div class="form-group">
                    <label for="">Tipo</label>
                    <input class="form-control" type="text" name="tipo" >
                </div>
                    <label for="">Data</label>
                    <input class="form-control" type="text" name="data" ><br>
                <div class="form-group">
                    <input class="form-control" class="btn btn-default" type="submit" value="Enviar">
                </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>