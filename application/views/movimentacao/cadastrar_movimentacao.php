<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
      <a class="navbar-brand" href="<?= base_url('movimentacoes') ?>">Controle Financeiro</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ">
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('movimentacoes') ?>">Movimentações <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= getDadosUsuarioLogado()['nome'] ?></a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="<?= base_url('usuarios/logout') ?>">Sair</a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-6 offset-3"> 
                <h1>Cadastrar movimentação</h1>
                <?php echo validation_errors() ?>
                <?= $this->session->flashdata('cadastro-movimentacao') ?>
                <form enctype="multipart/form-data" action="cadastrar" method="post">
                    <div class="form-group">
                        <label>Descrição</label>
                        <input class="form-control" name="descricao"/>
                    </div>
                    <div class="form-group">
                        <label>Valor</label>
                        <input class="form-control" name="valor"/>
                    </div>
                    <div class="form-group">
                        <label>Tipo</label><br>
                        <div class="form-check-inline">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" value="E" name="tipo">Entrada
                          </label>
                        </div>
                        <div class="form-check-inline">
                          <label class="form-check-label">
                            <input type="radio" class="form-check-input" value="S" name="tipo">Saída
                          </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Data</label>
                        <input type="text" class="form-control" name="data"/>
                    </div>
                    <div class="form-group">
                        <label>Comprovante</label>
                        <input type="file" name="comprovante" class="form-control" />
                    </div>
                    <div class="form-group">
                        <input type="submit" class="form-control" class="btn btn-default" value="enviar"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>