<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Recuperar senha</title>
</head>
<body>
    <div class="container">
        <div class="col-lg-6 offset-3">
            <h1>Recuperar senha</h1>
            <?= validation_errors() ?>
            <?= $this->session->flashdata('recuperar-senha') ?>
            <?= form_open(base_url('usuarios/recuperar-senha')) ?>
            <div class="form-group">
                <label>E-mail</label>
                <input type="email" name="email" maxlength="100" class="form-control" required />
            </div>
            <button class="btn btn-success btn-block">Enviar</button>
            <?= form_close() ?>
        </div>
    </div>
</body>
</html>