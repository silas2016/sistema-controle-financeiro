<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Redefinir senha</title>
</head>
<body>
    <div class="container">
        <div class="col-lg-6 offset-3">
            <h1>Redefinir senha</h1>
            <?= validation_errors() ?>
            <?= $this->session->flashdata('redefinir-senha') ?>
            <?php if ($tokenValido) { ?>
            <?= form_open(base_url("usuarios/redefinir-senha/{$token}")) ?>
            <div class="form-group">
                <label>Nova senha</label>
                <input type="password" name="senha" maxlength="100" class="form-control" required />
            </div>
            <button class="btn btn-success btn-block">Enviar</button>
            <?= form_close() ?>
            <?php } else { ?>
            <p class="alert alert-danger">Token de redefinição de senha inválido.</p>
            <?php } ?>
        </div>
    </div>
</body>
</html>